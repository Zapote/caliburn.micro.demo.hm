﻿namespace IssueTracker.WebService.Contracts
{
    public class IssueStatus
    {
        public const string Open = "Open";
        public const string Resolved = "Resolved";
    }
}