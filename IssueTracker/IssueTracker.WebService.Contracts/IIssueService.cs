﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace IssueTracker.WebService.Contracts
{
    [ServiceContract]
    public interface IIssueService
    {
        [OperationContract]
        void Create(Guid id, string titel, string text, string type);
        [OperationContract]
        void Update(Guid id, string titel, string text, string type);
        [OperationContract]
        void Resolve(Guid id);
        [OperationContract]
        IEnumerable<Issue> List();
    }
}
