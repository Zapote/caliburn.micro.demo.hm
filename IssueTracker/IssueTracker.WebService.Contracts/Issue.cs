﻿using System;

namespace IssueTracker.WebService.Contracts
{
    public class Issue
    {
        public Guid Id { get; set; }
        public string Titel { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }

        public override string ToString()
        {
            return string.Format("{0} '{1}' {2}", Type, Titel, Status);
        }
    }
}