﻿namespace IssueTracker.WebService.Contracts
{
    public class IssueType
    {
        public const string Bug = "Bug";
        public const string Task = "Task";
        public const string Proposal = "Proposal";
    }
}