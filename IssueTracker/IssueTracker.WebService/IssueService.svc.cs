﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using IssueTracker.WebService.Contracts;

namespace IssueTracker.WebService
{
    public class IssueService : IIssueService
    {
        private static readonly List<Issue> issues = new List<Issue>();

        public void Create(Guid id, string titel, string text, string type)
        {
            issues.Add(new Issue
            {
                Id = id,
                Titel = titel,
                Text = text,
                Type = type,
                Status = IssueStatus.Open
            });
        }

        public void Update(Guid id, string titel, string text, string type)
        {
            var issue = issues.FirstOrDefault(x => x.Id.Equals(id));
            if (issue == null) return;
            issue.Text = text;
            issue.Titel = titel;
            issue.Type = type;
        }

        public void Resolve(Guid id)
        {
            var issue = issues.FirstOrDefault(x => x.Id.Equals(id));
            if (issue == null) return;
            issue.Status = IssueStatus.Resolved;
        }

        public IEnumerable<Issue> List()
        {
            Thread.Sleep(5000);
            return issues;
        }
    }
}
