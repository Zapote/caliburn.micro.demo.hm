﻿using Caliburn.Micro;

namespace IssueTracker.Client.Infrastructure
{
    public interface IViewModelFactory
    {
        Screen GetScreen(string name);
        Screen GetScreen<T>();
    }
}