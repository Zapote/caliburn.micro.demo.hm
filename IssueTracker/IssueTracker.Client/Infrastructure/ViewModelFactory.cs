﻿using System;
using Caliburn.Micro;
using StructureMap;

namespace IssueTracker.Client.Infrastructure
{
    public class ViewModelFactory : IViewModelFactory
    {
        private readonly IContainer container;

        public ViewModelFactory(IContainer container)
        {
            if (container == null) throw new ArgumentNullException("container");
            this.container = container;
        }

        public Screen GetScreen(string name)
        {
            return container.TryGetInstance<Screen>(name);
        }

        public Screen GetScreen<T>()
        {
            return container.TryGetInstance(typeof(T)) as Screen;
        }
    }
}
