﻿using System;
using System.Windows;
using Caliburn.Micro;
using IssueTracker.Client.Infrastructure;
using IssueTracker.Client.Views;

namespace IssueTracker.Client.ViewModels
{
    public class ShellViewModel : Screen
    {
        private readonly IViewModelFactory viewModelFactory;

        public ShellViewModel(IViewModelFactory viewModelFactory)
        {
            if (viewModelFactory == null) throw new ArgumentNullException("viewModelFactory");
            this.viewModelFactory = viewModelFactory;
            IssuesList = viewModelFactory.GetScreen("ListIssues");
            EditIssue = viewModelFactory.GetScreen("EditIssue");
        }

        protected override void OnInitialize()
        {
            DisplayName = "IssueTracker";
        }

        public Screen IssuesList { get; set; }
        public Screen EditIssue { get; set; }
    }
}
