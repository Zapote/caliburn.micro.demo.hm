﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Caliburn.Micro;
using IssueTracker.Client.Messages;
using IssueTracker.WebService.Contracts;

namespace IssueTracker.Client.ViewModels
{
    public class ListIssuesViewModel : Screen, IHandle<IssueCreated>
    {
        private readonly IIssueService issueService;
        private readonly IEventAggregator eventAggregator;

        public ListIssuesViewModel(IIssueService issueService, IEventAggregator eventAggregator)
        {
            if (issueService == null) throw new ArgumentNullException("issueService");
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.issueService = issueService;
            this.eventAggregator = eventAggregator;

            eventAggregator.Subscribe(this);

            Issues = new BindableCollection<Issue>();
        }

        private void LoadIssues()
        {

            var issues = issueService.List();
            Issues.Clear();
            Issues.AddRange(issues);
        }

        public IObservableCollection<Issue> Issues { get; set; }

        public void RefreshList()
        {
            new TaskFactory().StartNew(LoadIssues);
        }


        public void Handle(IssueCreated message)
        {
            Issues.Add(new Issue
            {
                Id = message.Id,
                Titel = message.Title,
                Type = message.Type
            });
        }
    }
}