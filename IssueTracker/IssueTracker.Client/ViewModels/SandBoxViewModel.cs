﻿using System;
using Caliburn.Micro;
using IssueTracker.Client.Views;

namespace IssueTracker.Client.ViewModels
{
    public class SandBoxViewModel : Screen
    {
        private string message;
        private string name;
        private SandBoxView myView;


        protected override void OnViewAttached(object view, object context)
        {
            myView = (SandBoxView)view;
        }

        public string Message
        {
            get { return message; }
            set
            {
                if (value == message) return;
                message = value;
                NotifyOfPropertyChange(() => Message);
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (value == name) return;
                name = value;
                NotifyOfPropertyChange(() => Name);
                NotifyOfPropertyChange(() => CanUpdate);
            }
        }

        public bool CanUpdate
        {
            get
            {
                return !string.IsNullOrEmpty(Name);
            }
        }

        public void Update()
        {
            Message = "Hello" + Name;
            myView.Message.Content = "Kalle";
        }
    }
}
