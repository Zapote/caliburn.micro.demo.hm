﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using IssueTracker.Client.Messages;
using IssueTracker.WebService.Contracts;

namespace IssueTracker.Client.ViewModels
{
    public class EditIssueViewModel : Screen
    {
        private readonly IIssueService issueService;
        private readonly IEventAggregator eventAggregator;
        private string title;
        private string text;
        private string type;

        public EditIssueViewModel(IIssueService issueService, IEventAggregator eventAggregator)
        {
            if (issueService == null) throw new ArgumentNullException("issueService");
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            this.issueService = issueService;
            this.eventAggregator = eventAggregator;

            IssueTypes = new List<string>
            {
                IssueType.Bug,
                IssueType.Proposal,
                IssueType.Task,
            };

            Type = IssueTypes.First();
        }

        public string Title
        {
            get { return title; }
            set
            {
                if (value == title) return;
                title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }

        public string Text
        {
            get { return text; }
            set
            {
                if (value == text) return;
                text = value;
                NotifyOfPropertyChange(() => Text);
            }
        }

        public string Type
        {
            get { return type; }
            set
            {
                if (value == type) return;
                type = value;
                NotifyOfPropertyChange(() => Type);
            }
        }

        public IEnumerable<string> IssueTypes { get; set; }

        public void Save()
        {
            var id = Guid.NewGuid();
            issueService.Create(id, Title, Text, Type);
            eventAggregator.PublishOnUIThread(new IssueCreated
            {
                Id = id, Title = Title,Type = Type
            });
        }
    }
}
