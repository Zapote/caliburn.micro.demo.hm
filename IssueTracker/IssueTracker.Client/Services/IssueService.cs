﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using IssueTracker.WebService.Contracts;

namespace IssueTracker.Client.Services
{
    public class IssueService : ClientBase<IIssueService>, IIssueService
    {
        public IssueService()
            : base("IssueService")
        {

        }

        public void Create(Guid id, string titel, string text, string type)
        {
            try
            {
                Channel.Create(id, titel, text, type);
            }
            catch (Exception ex)
            {
                //publish error oinfo on eventaggregator
            }
        }

        public void Update(Guid id, string titel, string text, string type)
        {
            Channel.Update(id, titel, text, type);
        }

        public void Resolve(Guid id)
        {
            Channel.Resolve(id);
        }

        public IEnumerable<Issue> List()
        {
            return Channel.List();
        }
    }
}
