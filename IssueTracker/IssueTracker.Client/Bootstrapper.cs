﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using IssueTracker.Client.Infrastructure;
using IssueTracker.Client.Services;
using IssueTracker.Client.ViewModels;
using IssueTracker.WebService.Contracts;
using StructureMap;
using StructureMap.Graph;

namespace IssueTracker.Client
{
    public class Bootstrapper : BootstrapperBase
    {
        private IContainer container;

        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

        protected override void Configure()
        {
            container = new Container(x =>
            {
                x.For<IWindowManager>().Use<WindowManager>();
                x.For<IEventAggregator>().Singleton().Use<EventAggregator>();
                x.For<IIssueService>().Use<IssueService>();
                x.For<IViewModelFactory>().Use<ViewModelFactory>();
                x.Scan(s =>
                {
                    s.AddAllTypesOf<Screen>().NameBy(t => t.Name.Replace("ViewModel", ""));
                    s.TheCallingAssembly();
                });
            });
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            return string.IsNullOrEmpty(key)
                       ? container.GetInstance(serviceType)
                       : container.GetInstance(serviceType ?? typeof(object), key);
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return container.GetAllInstances(serviceType).Cast<object>();
        }

        protected override void BuildUp(object instance)
        {
            container.BuildUp(instance);
        }
    }
}
