using System;

namespace IssueTracker.Client.Messages
{
    public class IssueCreated
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public Guid Id { get; set; }
    }
}